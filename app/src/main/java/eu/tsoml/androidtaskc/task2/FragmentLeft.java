package eu.tsoml.androidtaskc.task2;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import eu.tsoml.androidtaskc.R;

public class FragmentLeft extends Fragment {

    private EditText decimalEditText;
    private EditText binaryEditText;
    private EditText octalEditText;
    private EditText hexadecimalEditText;

    private FragmentLeftListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (FragmentLeftListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_left_task2, container, false);

        decimalEditText = fragmentView.findViewById(R.id.decimalEditText);
        decimalEditText.addTextChangedListener(new DataTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!decimalEditText.getText().toString().equals("")) {
                    String decimal = decimalEditText.getText().toString();
                    listener.onShareData(decimal, NumberCategory.DECIMAL);
                }

            }
        });

        binaryEditText = fragmentView.findViewById(R.id.binaryEditText);
        binaryEditText.addTextChangedListener(new DataTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!binaryEditText.getText().toString().equals("")) {
                    String binary = binaryEditText.getText().toString();
                    listener.onShareData(binary, NumberCategory.BINARY);
                }
            }
        });

        octalEditText = fragmentView.findViewById(R.id.octalEditText);
        octalEditText.addTextChangedListener(new DataTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!octalEditText.getText().toString().equals("")) {
                    String octal = octalEditText.getText().toString();
                    listener.onShareData(octal, NumberCategory.OCTAL);
                }

            }
        });

        hexadecimalEditText = fragmentView.findViewById(R.id.hexadecimalEditText);
        hexadecimalEditText.addTextChangedListener(new DataTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                if (!hexadecimalEditText.getText().toString().equals("")) {
                    String hexadecimal = hexadecimalEditText.getText().toString();
                    listener.onShareData(hexadecimal, NumberCategory.HEX);
                }

            }
        });
        return fragmentView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }


    public interface FragmentLeftListener {
        void onShareData(String number, NumberCategory type);
    }

}
