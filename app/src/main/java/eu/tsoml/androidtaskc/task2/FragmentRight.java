package eu.tsoml.androidtaskc.task2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import eu.tsoml.androidtaskc.R;

public class FragmentRight extends Fragment {

    private TextView decimalTextView;
    private TextView binaryTextView;
    private TextView octalTextView;
    private TextView hexadecimalTextView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_right_task2, container, false);

        decimalTextView = fragmentView.findViewById(R.id.decimalTextView);
        binaryTextView = fragmentView.findViewById(R.id.binaryTextView);
        octalTextView = fragmentView.findViewById(R.id.octalTextView);
        hexadecimalTextView = fragmentView.findViewById(R.id.hexadecimalTextView);

        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    void getData(String number, NumberCategory type) {

        Number num = new Number();


        if (type == NumberCategory.DECIMAL) {

            setData(number);
        }

        if (type == NumberCategory.BINARY) {
            number = String.valueOf(num.binaryToDecimal(number));
            setData(number);
        }

        if (type == NumberCategory.OCTAL) {
            number = String.valueOf(num.octalToDecimal(number));
            setData(number);
        }

        if (type == NumberCategory.HEX) {
            number = String.valueOf(num.hexToDecimal(number));
            setData(number);
        }
    }

    void setData(String number) {
        Number num = new Number();
        decimalTextView.setText(String.format("Decimal: %s", number));
        binaryTextView.setText(String.format("Binary: %s", num.decimalToBinary(number)));
        octalTextView.setText(String.format("Octal: %s", num.decimalToOctal(number)));
        hexadecimalTextView.setText(String.format("Hex: %s", num.decimalToHex(number)));
    }
}
