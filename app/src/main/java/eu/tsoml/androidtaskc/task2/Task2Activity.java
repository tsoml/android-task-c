package eu.tsoml.androidtaskc.task2;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import eu.tsoml.androidtaskc.R;

public class Task2Activity extends AppCompatActivity implements FragmentLeft.FragmentLeftListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);
    }

    @Override
    public void onShareData(String number, NumberCategory type) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentRight fragmentRight = (FragmentRight) fragmentManager.findFragmentById(R.id.fragment_right);

        if (fragmentRight != null) {
            fragmentRight.getData(number, type);
        }
    }
}
