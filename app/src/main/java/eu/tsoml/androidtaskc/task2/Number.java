package eu.tsoml.androidtaskc.task2;


class Number {


    Number() {

    }

    String decimalToBinary(String decimal) {
        return Integer.toBinaryString(Integer.parseInt(decimal));
    }

    String decimalToOctal(String decimal) {
        return Integer.toOctalString(Integer.parseInt(decimal));
    }

    String decimalToHex(String decimal) {
        return Integer.toHexString(Integer.parseInt(decimal));
    }


    int binaryToDecimal(String binary) {
        return Integer.parseInt(String.valueOf(binary), 2);
    }


    int octalToDecimal(String octal) {
        return Integer.parseInt(String.valueOf(octal), 8);
    }

    int hexToDecimal(String hex) {
        return Integer.parseInt(String.valueOf(hex), 16);
    }


}
