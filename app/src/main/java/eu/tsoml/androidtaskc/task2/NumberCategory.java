package eu.tsoml.androidtaskc.task2;

public enum NumberCategory {
    DECIMAL, BINARY, OCTAL, HEX;
}