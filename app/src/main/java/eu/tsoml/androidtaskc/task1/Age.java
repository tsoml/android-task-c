package eu.tsoml.androidtaskc.task1;

class Age {
    private int days;
    private int months;
    private int years;

    Age(int days, int months, int years) {
        this.days = days;
        this.months = months;
        this.years = years;
    }

    int getDays() {
        return this.days;
    }

    int getMonths() {
        return this.months;
    }

    int getYears() {
        return this.years;
    }
}
