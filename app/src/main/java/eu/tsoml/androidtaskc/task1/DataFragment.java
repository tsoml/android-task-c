package eu.tsoml.androidtaskc.task1;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;


import eu.tsoml.androidtaskc.R;

public class DataFragment extends Fragment {
    private final static String STATE_KEY_USERNAME = "username";
    private final static String STATE_KEY_YEAR = "year";
    private final static String STATE_KEY_MONTH = "month";
    private final static String STATE_KEY_DAY = "day";

    private Button gotoNextScreenButton;
    private EditText usernameEditText;
    private DatePicker datePicker;
    private int year;
    private int month;
    private int day;
    private DataFragmentListener listener;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        listener = (DataFragmentListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_data_task1, container, false);
        usernameEditText = fragmentView.findViewById(R.id.usernameEditText);
        datePicker = fragmentView.findViewById(R.id.date_picker);

        gotoNextScreenButton = fragmentView.findViewById(R.id.gotoNextScreenButton);
        gotoNextScreenButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    String username = usernameEditText.getText().toString();

                    year = datePicker.getYear();
                    month = datePicker.getMonth();
                    day = datePicker.getDayOfMonth();

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, day);

                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
                    String date = format.format(calendar.getTime());

                    listener.onNavigateToNextScreen(username, date);
                }
            }
        });
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            usernameEditText.setText(savedInstanceState.getString(STATE_KEY_USERNAME));
            datePicker.updateDate(savedInstanceState.getInt(STATE_KEY_YEAR),
                    savedInstanceState.getInt(STATE_KEY_MONTH),
                    savedInstanceState.getInt(STATE_KEY_DAY));
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_KEY_USERNAME, usernameEditText.getText().toString());
        outState.putInt(STATE_KEY_YEAR, datePicker.getYear());
        outState.putInt(STATE_KEY_MONTH, datePicker.getMonth());
        outState.putInt(STATE_KEY_DAY, datePicker.getDayOfMonth());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    interface DataFragmentListener {
        void onNavigateToNextScreen(String username, String date);
    }
}
