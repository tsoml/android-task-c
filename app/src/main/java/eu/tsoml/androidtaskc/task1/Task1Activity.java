package eu.tsoml.androidtaskc.task1;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import eu.tsoml.androidtaskc.R;

public class Task1Activity extends AppCompatActivity implements DataFragment.DataFragmentListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task1);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.fragmentContainer, new DataFragment(), "fragment")
                    .commit();
        }

    }

    @Override
    public void onNavigateToNextScreen(String username, String date) {
        MainFragment fragment = MainFragment.getInstance(username, date);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment, "fragment")
                .addToBackStack(null)
                .commit();
    }


}
