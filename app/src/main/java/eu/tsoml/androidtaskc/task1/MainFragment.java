package eu.tsoml.androidtaskc.task1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import eu.tsoml.androidtaskc.R;

import static eu.tsoml.androidtaskc.task1.AgeCalculator.calculateAge;


public class MainFragment extends Fragment {

    private static final String ARG_USERNAME = "username";
    private static final String ARG_DATE_OF_BIRTH = "date";

    private TextView usernameTextView;
    private TextView yearsTextView;
    private TextView monthsTextView;
    private TextView daysTextView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_main_task1, container, false);
        usernameTextView = fragmentView.findViewById(R.id.tv_username);
        yearsTextView = fragmentView.findViewById(R.id.tv_years);
        monthsTextView = fragmentView.findViewById(R.id.tv_months);
        daysTextView = fragmentView.findViewById(R.id.tv_days);
        return fragmentView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy", Locale.ENGLISH);
        sdf.applyPattern("ddMMyyyy");

        Date birthDate = null;
        try {
            birthDate = sdf.parse(getUserDateOfBirth());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Age age = calculateAge(birthDate);

        usernameTextView.setText(getUserName());
        yearsTextView.setText("Years: " + age.getYears());
        monthsTextView.setText("Months: " + age.getMonths());
        daysTextView.setText("Days: " + age.getDays());


    }

    private String getUserName() {
        return getArguments().getString(ARG_USERNAME);
    }


    private String getUserDateOfBirth() {
        return getArguments().getString(ARG_DATE_OF_BIRTH);
    }

    static MainFragment getInstance(String username, String date) {
        Bundle arguments = new Bundle();
        arguments.putString(ARG_USERNAME, username);
        arguments.putString(ARG_DATE_OF_BIRTH, date);

        MainFragment fragment = new MainFragment();
        fragment.setArguments(arguments);

        return fragment;
    }
}